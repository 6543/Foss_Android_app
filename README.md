
# FOSS apps on android
**App store: Fdroid**

## Propietary front end app alternatives

|Proprietary front ends|Foss front ends|
|----------------------|---------------|
|Play store|[Aurora Droid](https://f-droid.org/packages/com.aurora.store) <sup>**[[F-droid](https://www.f-droid.org/)]**</sup>|
|[Youtube](https://play.google.com/store/apps/details?id=com.google.android.youtube&referrer=utm_source%3Dgoogle%26utm_medium%3Dorganic%26utm_term%3Dyoutube&pcampaignid=APPU_1_CUGuYLTAA9etoATQoqrgDw)|[Newpipe](https://github.com/TeamNewPipe/NewPipe)|
|[Twitch](https://play.google.com/store/apps/details?id=tv.twitch.android.app&hl=en&gl=US&referrer=utm_source%3Dgoogle%26utm_medium%3Dorganic%26utm_term%3Dtwitch&pcampaignid=APPU_1_eEGuYLiPEdWB-QbLpLbgDw)|[twire](https://f-droid.org/packages/com.perflyst.twire) <sup>**[[Fdroid](https://www.f-droid.org/)]**</sup>|
|[Twitter](https://play.google.com/store/apps/details?id=com.twitter.android&hl=en_US&gl=US&referrer=utm_source%3Dgoogle%26utm_medium%3Dorganic%26utm_term%3Dtwitter&pcampaignid=APPU_1_70GuYKvgDbCh-QbZ9bK4AQ)|[fritter](https://f-droid.org/packages/com.jonjomckay.fritter) <sup>**[[Fdroid](https://www.f-droid.org/)]**</sup>|
|[Telegram](https://play.google.com/store/apps/details?id=org.telegram.messenger&hl=en_US&gl=US&referrer=utm_source%3Dgoogle%26utm_medium%3Dorganic%26utm_term%3Dtelegram&pcampaignid=APPU_1_6TyuYKGAIMG9hwPxlKOIBA)|[Telegram foss](https://f-droid.org/packages/org.telegram.messenger) <sup>**[[Fdroid](https://www.f-droid.org/)]**</sup> or [Nekogram X](https://f-droid.org/packages/nekox.messenger) <sup>**[[Fdroid](https://www.f-droid.org/)]**</sup>|
|[Facebook](https://play.google.com/store/apps/details?id=com.facebook.katana&hl=en&gl=US&referrer=utm_source%3Dgoogle%26utm_medium%3Dorganic%26utm_term%3Dfacebook&pcampaignid=APPU_1_n1uuYKDiA-LjrgTIrpmQAw)|[SlimSocial](https://f-droid.org/packages/it.rignanese.leo.slimfacebook) <sup>**[[Fdroid](https://www.f-droid.org/)]**</sup> or [frost](https://f-droid.org/packages/com.pitchedapps.frost) <sup>**[[Fdroid](https://www.f-droid.org/)]**</sup> |
|[Instagram](https://play.google.com/store/apps/details?id=com.instagram.android&hl=en_IN&gl=US&referrer=utm_source%3Dgoogle%26utm_medium%3Dorganic%26utm_term%3Dinstagram&pcampaignid=APPU_1__1uuYOyDF86QrgT8n7H4BQ)|[Barinsta](https://f-droid.org/packages/me.austinhuang.instagrabber) <sup>**[[Fdroid](https://www.f-droid.org/)]**</sup> |
|[Reddit](https://play.google.com/store/apps/details?id=com.reddit.frontpage&hl=en&gl=US&referrer=utm_source%3Dgoogle%26utm_medium%3Dorganic%26utm_term%3Dreddit&pcampaignid=APPU_1_p1yuYLi4IMTxhwPU3KKgBg)|[Stealth](https://f-droid.org/packages/com.cosmos.unreddit) <sup>**[[Fdroid](https://www.f-droid.org/)]**</sup> or [Infinity](https://f-droid.org/packages/ml.docilealligator.infinityforreddit) <sup>**[[Fdroid](https://www.f-droid.org/)]**</sup>|








## Google App and Social Media Replacements
|GApps and Social Media|Federated or FOSS alternatives|
|----------------------|------------------------------|
|[Hangouts](https://play.google.com/store/apps/details?id=com.google.android.talk&hl=en&gl=US&referrer=utm_source%3Dgoogle%26utm_medium%3Dorganic%26utm_term%3Dhangouts&pcampaignid=APPU_1_4WCuYPyrGc7i-AbLtKOQDw) <sup>**[[Play Store](https://play.google.com/)]**</sup>|[Signal](https://play.google.com/store/apps/details?id=org.thoughtcrime.securesms&hl=en_US&gl=US&referrer=utm_source%3Dgoogle%26utm_medium%3Dorganic%26utm_term%3Dsignal&pcampaignid=APPU_1_Y2GuYICLOdD6wQO0tL_QCQ) <sup>**[[Play Store](https://play.google.com/)]**</sup> (under some problems due to cryptocurrency) or [Session](https://github.com/oxen-io/session-android)|
|[Maps](https://play.google.com/store/apps/details?id=com.google.android.apps.maps&hl=en&gl=US&referrer=utm_source%3Dgoogle%26utm_medium%3Dorganic%26utm_term%3Dgoogle+maps&pcampaignid=APPU_1_I2KuYPD_J5X7wQO47pa4DQ) <sup>**[[Play Store](https://play.google.com/)]**</sup>|[OpenStreetMaps](https://f-droid.org/en/packages/net.osmand.plus/)|
|[Youtube](https://www.youtube.com/)|[Peertube](https://joinpeertube.org/) or [LBRY](https://lbry.com/). Or Invidious for youtube videos without google tracking|
|[Twitter](https://twitter.com/?lang=en)|[Mastodon](https://joinmastodon.org/)|
|[Facebook](https://www.facebook.com/)|[Mastodon](https://joinmastodon.org/)/[Diaspora](https://diasporafoundation.org/)|
|[Instagram](https://www.instagram.com/)|[Pixelfed](https://pixelfed.social/)/[Plermora](https://pleroma.social/)|
|Google Source code developement or [Github](https://github.com/)|[Gitlab](https://about.gitlab.com/) or [Codeberg](https://codeberg.org/)|
|Google Photos <sup>**[[Play Store](https://play.google.com/)]**</sup>|[Stingle](https://f-droid.org/en/packages/org.stingle.photos/) <sup>**[[Fdroid](https://www.f-droid.org/)**</sup>|
|[Google Play](https://play.google.com/)]|[Fdroid](https://www.f-droid.org/)|
|[Reddit](https://www.reddit.com/)|[Lemmy](https://join.lemmy.ml/)|
|[Google Meet](https://play.google.com/store/apps/details?id=com.google.android.apps.meetings&hl=en_US&gl=US&referrer=utm_source%3Dgoogle%26utm_medium%3Dorganic%26utm_term%3Dgoogle+meet&pcampaignid=APPU_1_VCmxYP3EIcSSwPAPl-ikoAQ) <sup>**[[Play Store](https://play.google.com/)**</sup>/[Zoom](https://www.google.com/search?q=zoom&safe=strict&ei=HiqxYKj3F4HurgTmsYb4Bg&oq=zoom&gs_lcp=ChNtb2JpbGUtZ3dzLXdpei1zZXJwEAMyBAgAEEcyBAgAEEcyBAgAEEcyBAgAEEcyBAgAEEcyBAgAEEcyBAgAEEcyBAgAEEdQwgdYwgdgvghoAHABeACAAQCIAQCSAQCYAQCgAQHIAQjAAQE&sclient=mobile-gws-wiz-serp#) <sup>**[[Play Store](https://play.google.com)**</sup>|[Jitsi Meet](https://f-droid.org/en/packages/org.jitsi.meet/) <sup>**[[Fdroid](https://www.f-droid.org/)]**</sup>|
|[Google Drive]|Nextcloudfor self host or Mega for cloud storage (35 gb is only for first month then it dissapears so its only 15 gb)|
|Google Notes|Nextcloud notes, etesync or standard notes|
|Google Tasks|SimpleTasks or Nextcloud Tasks|
|Whatsapp|Signal/Session, or Jami|
|Google Camera|Open Camera|
|Google Translate|Libre Translate|
|Google Chrome|Firefox or Bromite. Ya know firefox. Bromite is an privacy version of chrome. Try it. And tor is the safest browser on android. Firefox on android is not great. I would reccomend on fenix on fdroid which is a fork of firefox but it has less trackers as compared to firefox|
|Microsoft word/Excel/powerpoint|Collabora Office on mobile. LibreOffice on desktop|
|Gboard|AnySoftKeyboard or FlorisBoard. Pick your poison|
|Dischord|Matrix.org. Android client element|
|Gmail|ProtonMail|

## FOSS apps you should try available on fdroid

|App|My personal description|
|---|-----------------------|
|Tachiyomi <sup>**[[Fdroid](https://www.f-droid.org/)]**</sup>  |Manga Reader|
|Kotatsu <sup>**[[Fdroid](https://www.f-droid.org/)]**</sup>  |Manga Reader|
|Loop Habit Tracker <sup>**[[Fdroid](https://www.f-droid.org/)]**</sup>  |Your habit tracking app. Start your new habit with this.|
|Noice <sup>**[[Fdroid](https://www.f-droid.org/)]**</sup> |A relaxing nature sound app. Create your own nature using the sounds in this app|
|Anki Droid <sup>**[[Fdroid](https://www.f-droid.org/)]**</sup>  |A flashcard app which helps students study|
|App Manager <sup>**[[Fdroid](https://www.f-droid.org/)]**</sup> |An app list of all apps on your device|
|Package manager <sup>**[[Fdroid](https://www.f-droid.org/)]**</sup> |Also an app list but can also have powerful features if given root|
|Tracker Control <sup>**[[Fdroid](https://www.f-droid.org/)]**</sup> |Fed up of your apps having trackers. Use this app to block trackers, without root this acts as a vpn|
|Netguard <sup>**[[Fdroid](https://www.f-droid.org/)]**</sup>  |An app firewall which blocks app from accessing the internet. You can be happy knowing that apps that don't need internet is blocked :)|
|Converter Now <sup>**[[Fdroid](https://www.f-droid.org/)]**</sup>  |Unit changing app|
|Unit Converter Ultimate <sup>**[[Fdroid](https://www.f-droid.org/)]**</sup>  |Unit changing app|
|Krita <sup>**[[Fdroid](https://www.f-droid.org/)]**</sup> |(Still in development) Drawing app. Draw whatever you want|
|Tusky <sup>**[[Fdroid](https://www.f-droid.org/)]**</sup> |You cute front end buddy for mastodon|
|Memetastic <sup>**[[Fdroid](https://www.f-droid.org/)]**</sup>|Gotta make those memes. Spicy memes. Yes this is a meme maker. Ya know you want it|
|Exodus <sup>**[[Fdroid](https://www.f-droid.org/)]**</sup>  |Tracker finding app which finds trackers in apps which uses online database|
|Classyshark3xodus <sup>**[[Fdroid](https://www.f-droid.org/)]**</sup>| Same thing but a little offline|
|Docus <sup>**[[Fdroid](https://www.f-droid.org/)]**</sup>|Document Scanner. Your gonna lose your passport if you carry it around. Better scan it|
|Coffee <sup>**[[Fdroid](https://www.f-droid.org/)]**</sup>|Just like your coffee makes you stand up at night, this app makes sure you device ain't gonna go to sleeping mode| 
|Antennapod <sup>**[[Fdroid](https://www.f-droid.org/)]**</sup>|Podcast app. Listen to your podcast and subscribe to them|
|VLC <sup>**[[Fdroid](https://www.f-droid.org/)]**</sup> |Your video player. Definitely better than the default one|
|ScreenCam <sup>**[[Fdroid](https://www.f-droid.org/)]**</sup>|Screen Recorder. You don't have to worry about ads or that annoying watermark. You can do without watermark|
|Scrambled Exif <sup>**[[Fdroid](https://www.f-droid.org/)]**</sup>|Did you know that all photos you share in social media have data which help in finding which device and where the photo was taken? That's what this app is for. It helps in removing your exif data|
|Briar <sup>**[[Fdroid](https://www.f-droid.org/)]**</sup>|If the NSA or the FBI try to catch you and you want to share info to your journalists friends, then use this app. Its a messaging app which is total anonymity|
|Imagepipe <sup>**[[Fdroid](https://www.f-droid.org/)]**</sup>|An app which helps is making your photos less quality so to make it smaller to send. Who want 4k photos on whatsapp? (Cries in lonliness). Oh yeah it also removes your exif data|
|OpenTasks <sup>**[[Fdroid](https://www.f-droid.org/)]**</sup>|Task Manager app in which you can use deadline, progress and lots of other features|
|Forkus <sup>**[[Fdroid](https://www.f-droid.org/)]**</sup>|Students, this is for you. This task app is meant for you|
|Pulse Music <sup>**[[Fdroid](https://www.f-droid.org/)]**</sup>|A good audio player|
|Tickmate <sup>**[[Fdroid](https://www.f-droid.org/)]**</sup>|Basically if you did a stuff in a day, you can use this journal to tick that you did that action that day. Its UI is old. But its use cases are kinda good|
|LibreSpeed <sup>**[[Fdroid](https://www.f-droid.org/)]**</sup>|Check your internet speed. Don't worry Jimmy, you will always have kb/s internet speed no matter which app you try|
|Xournal++ <sup>**[[Fdroid](https://www.f-droid.org/)]**</sup>|This is a S note like app for people who have a pen for their tablets and are not like us plebs.
|Simple Mobile Tools <sup>**[[Fdroid](https://www.f-droid.org/)]**</sup>|This includes simple file manager, simple gallery etc. This is a replacement for stock file or gallery or etc. Made by a the same people|
|AnonAddy <sup>**[[Fdroid](https://www.f-droid.org/)]**</sup>|An alias email maker. Want to try out that sketchy website and see what all notifications you get but are afraid of our email being leaked in a server leak. Then use this. This gives you an alias email connected to an original email. So you can still get notifications and still have the option to remove the alias if it was leaked. So yeah|
|SimpleLogin <sup>**[[Fdroid](https://www.f-droid.org/)]**</sup>|Same as Anonaddy except there is a limit to how much alias you can make|
|Fedilab <sup>**[[Fdroid](https://www.f-droid.org/)]**</sup>|For people who are too lazy to download and handle many of my google and propietary alternative to social media, use this. It will connect you to the fediverse. Which includes mastodon,plemora,pixelfed etc|
|Goodtime <sup>**[[Fdroid](https://www.f-droid.org/)]**</sup>|Basically its like a timer for your productivity. Depending on your configuration, hours of works equals minutes of break. So for each hour, a 10 minute break. Something like that|
|World Scribe <sup>**[[Fdroid](https://www.f-droid.org/)]**</sup>|This is a writer app which helps in remembering your characters which you created and their characteristics. Its meant for writers|
|Ffupdater <sup>**[[Fdroid](https://www.f-droid.org/)]**</sup>|If someone is way too lazy to go to the website and update firefox, here is an app for you. It helps in updating firefox and bromite as well|
|Apkgrabber <sup>**[[Fdroid](https://www.f-droid.org/)]**</sup>|Too lazy to check the google play store for updates. Well I got an app for you. This app finds update for your apps and you can download and install them|
|ProtonVPN <sup>**[[Fdroid](https://www.f-droid.org/)]**</sup>|This is a vpn service. Yes but it won't steal your data like hotspot shield. And the chinese government won't find you watching your “stuff”|
|Minetest <sup>**[[Fdroid](https://www.f-droid.org/)]**</sup>|Basically walmart version of minecraft
SealNotes <sup>**[[Fdroid](https://www.f-droid.org/)]**</sup>|Scared that the NSA or FBI is watching your notes, then use this app. It encrypts using AES 256 encryption. So you don't have to worry about that. Only problem is you have to unlock everytime you open the app. You can toggle the time it will take to autolock itself|


#Now time for drink some coffee. Peace
